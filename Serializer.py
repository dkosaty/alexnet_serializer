import os, re, numpy as np, cPickle
from PIL import Image, ImageOps
from scipy import misc

class Serializer:

    def __init__(self, options):
        self.dataset_path = options.dataset_path

        self.num_cases_per_batch = options.num_cases_per_batch

        if options.image_size is None:
            self.image_size = 32
        else:
            self.image_size = options.image_size

        self.batches_meta_name = 'batches.meta'

        train_batch_names, train_range = Serializer.parse_range(options.train_range)
        test_batch_names, test_range = Serializer.parse_range(options.test_range)
        self.samples = \
            {
                'training':
                    {
                        'range': train_range,
                        'size': train_range*self.num_cases_per_batch,
                        'batch_names': train_batch_names
                    },
                'testing':
                    {
                        'range': test_range,
                        'size': test_range*self.num_cases_per_batch,
                        'batch_names': test_batch_names
                    }
            }

        self.saving_folder = options.saving_folder

        self.read_files()

    @staticmethod
    def parse_range(range_type):
        pattern = r'(\d+)-?(\d*)'
        founded = re.search(pattern, range_type)
        if founded:
            if founded.endpos == 3:
                first, last = int(founded.group(1)), int(founded.group(2))
            else:
                first = int(founded.group(1))
                last = first
            batch_names = ['data_batch_'+str(num) for num in range(first, last+1)]
        else:
            raise ValueError, "Don't parse the ranges."
        return batch_names, last-first+1

    def read_files(self):
        try:
            training_positive_file = open(os.path.join(self.dataset_path, 'Train', 'pos.lst'), 'r')
            training_negative_file = open(os.path.join(self.dataset_path, 'Train', 'neg.lst'), 'r')
            test_positive_file = open(os.path.join(self.dataset_path, 'Test', 'pos.lst'), 'r')
            test_negative_file = open(os.path.join(self.dataset_path, 'Test', 'neg.lst'), 'r')

            self.readed_lines = \
                {
                    'training': {'pos': training_positive_file.readlines(), 'neg': training_negative_file.readlines()},
                    'testing': {'pos': test_positive_file.readlines(), 'neg': test_negative_file.readlines()}
                }
        except IOError as error:
            print 'IOError excepted: '.error.strerror
        finally:
            test_negative_file.close()
            test_positive_file.close()
            training_negative_file.close()
            training_positive_file.close()

    def create_batches_meta(self):
        def func(name):
            image = Image.open(name)
            image = image.resize((self.image_size, self.image_size))
            img_data = ImageOps.fit(image, (self.image_size, self.image_size))
            img_data = np.array(img_data)
            img_data = img_data.T.reshape(3,-1).reshape(-1)
            return img_data.astype(np.single)

        def get_names_and_labels_list(names_and_labels, set_type):
            to = self.samples[set_type]['size']/2

            for line in self.readed_lines[set_type]['pos'][:to]:
                names_and_labels.append((os.path.join(self.dataset_path, line[:len(line)-1]), 1))

            for line in self.readed_lines[set_type]['neg'][:to]:
                names_and_labels.append((os.path.join(self.dataset_path, line[:len(line)-1]), 0))

        names_and_labels = []

        get_names_and_labels_list(names_and_labels, 'training')

        get_names_and_labels_list(names_and_labels, 'testing')

        rows = [func(name) for name, label in names_and_labels]

        data = np.vstack([r for r in rows if r is not None])

        meta_dict = \
            {
                'num_cases_per_batch': self.num_cases_per_batch,
                'label_names': ['non_person', 'person'],
                'num_vis': 3*self.image_size*self.image_size,
                'data_mean': data.mean(axis=0)
            }

        with open(os.path.join(self.saving_folder, self.batches_meta_name), 'wb') as file:
            cPickle.dump(meta_dict, file)

    def serialize(self, set_type):
        for num in range(self.samples[set_type]['range']):
            self.create_data_batch(set_type, num)

    def create_data_batch(self, set_type, num):
        data, filenames = [], []

        start = num*self.samples[set_type]['size']/(2*self.samples[set_type]['range'])
        stop = (num+1)*self.samples[set_type]['size']/(2*self.samples[set_type]['range'])

        labels = [1]*(stop-start)

        self.processing_set\
            (
                lines=self.readed_lines[set_type]['pos'][start:stop],
                data=data,
                filenames=filenames
            )

        labels += [0]*(stop-start)

        self.processing_set\
            (
                lines=self.readed_lines[set_type]['neg'][start:stop],
                data=data,
                filenames=filenames
            )

        data_dict = \
            {
                'batch_label': set_type+' batch '+str(num+1)+' of '+str(self.samples[set_type]['range']),
                'labels': labels,
                'data': np.rot90(np.flipud(np.array(data)), k=3),
                'filenames': filenames
            }

        with open(os.path.join(self.saving_folder, self.samples[set_type]['batch_names'][num]), 'wb') as file:
            cPickle.dump(data_dict, file)

    def processing_set(self, lines, data, filenames):
        for line in lines:
            current_data = self.get_image(line[:len(line)-1])
            if current_data is None:
                raise ValueError, "The image doesn't opened."
            data.append((current_data.T).flatten())

            filenames.append(self.get_imagename(line))

    def get_image(self, path_to_image):
        image = Image.open(os.path.join(self.dataset_path, path_to_image))
        image = image.resize((self.image_size, self.image_size))
        # misc.imshow(image)
        return np.fliplr(np.rot90(np.array(image, order='C'), k=3))

    def get_imagename(self, line):
        pattern = r'\w*\.+\w+'
        founded = re.search(pattern, line)
        if founded:
            filename = founded.group(0)
            # print filename
        else:
            raise ValueError, "The pattern doesn't matched filename."
        return filename