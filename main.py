from optparse import OptionParser
from Serializer import *

def main():
    try:
        options = parse_command_line()

        if not os.path.exists(options.saving_folder):
            os.makedirs(options.saving_folder)

        serializer = Serializer(options)

        print 'serialize the batches meta'
        serializer.create_batches_meta()

        print 'serialize the training instances'
        serializer.serialize('training')

        print 'serialize the test instances'
        serializer.serialize('testing')

    except ValueError as error:
        print 'ValueError excepted: ',error.message
    except RuntimeError as error:
        print 'RuntimeError excepted: ',error.message

def parse_command_line():
    parser = OptionParser()

    parser.add_option('--dataset_path', dest='dataset_path', type='string')
    parser.add_option('--num_cases_per_batch', dest='num_cases_per_batch', type='int')
    parser.add_option('--image_size', dest='image_size', type='int')
    parser.add_option('--train_range', dest='train_range', type='string')
    parser.add_option('--test_range', dest='test_range', type='string')
    parser.add_option('--saving_folder', dest='saving_folder', type='string')

    (options, args) = parser.parse_args()

    return options

if __name__ == '__main__':
    main()