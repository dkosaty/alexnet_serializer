The data serializer for cuda-convnet library (https://code.google.com/p/cuda-convnet/).
The data have INRIA dataset view (http://pascal.inrialpes.fr/data/human/)

The typical use of the serializer.py is "--dataset_path=/home/dmitry/INRIAPerson --num_cases_per_batch=288 --train_range=1-4 --test_range=5-6 --saving_folder=inria_batches32x32".

The typical use of the test.py is "--train_range=1-4 --test_range=5-6 --saving_folder=inria_batches32x32".