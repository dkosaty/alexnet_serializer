from Serializer import *
from main import parse_command_line

def main():
    options = parse_command_line()

    test_serialization(options.saving_folder, 'batches.meta')

    train_batch_names, train_range = Serializer.parse_range(options.train_range)
    for num in range(train_range):
        test_serialization(options.saving_folder, train_batch_names[num])

    test_batch_names, test_range = Serializer.parse_range(options.test_range)
    for num in range(test_range):
        test_serialization(options.saving_folder, test_batch_names[num])

def test_serialization(saving_folder, batch_name):
    def unpickle(file):
        import cPickle
        fo = open(file, 'rb')
        dict = cPickle.load(fo)
        fo.close()
        return dict

    data = unpickle(os.path.join(saving_folder, batch_name))
    print data

if __name__ == '__main__':
    main()